import { Job } from './data-types';
/**
 * ジョブの登録
 *
 * FirestoreへのJob登録時ののIDは、モジュール名とタイムスタンプで一意化します
 *
 * @param job 実行したいアクションの情報
 */
export declare const dispatch: (job: Job) => Promise<void>;
