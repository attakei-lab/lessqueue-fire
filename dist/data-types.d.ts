import { DocumentData } from '@google-cloud/firestore';
/**
 * ジョブ実行のアクション定義
 */
export interface Job {
    /**
     * 実行ジョブのモジュール名
     */
    module: string;
    /**
     * ジョブ実行の追加パラメーター
     */
    payload?: {};
}
/**
 * ジョブとして定義されるモジュール
 */
export interface JobModule {
    name: string;
    description: string;
    handle(payload: any, context?: any): Promise<void>;
}
export interface JobSnapshot {
    id: string;
    data(): DocumentData | Job;
}
export interface RecieverOptions {
    modulePaths?: string[];
    jobCollectioName?: string;
}
