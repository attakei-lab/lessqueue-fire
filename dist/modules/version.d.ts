/*******************
 * バージョンをどこかにポストするだけのモジュール
 */
export declare const name = "@@version";
export declare const description = "\u30D0\u30FC\u30B8\u30E7\u30F3\u3092\u30B3\u30F3\u30BD\u30FC\u30EB\u51FA\u529B\u3059\u308B\u3060\u3051\u306E\u30E2\u30B8\u30E5\u30FC\u30EB";
export declare const handle: () => Promise<void>;
