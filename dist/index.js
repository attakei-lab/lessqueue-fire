"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Entrypoint for this package.
 *
 */
var dispatcher_1 = require("./dispatcher");
exports.dispatch = dispatcher_1.dispatch;
var reciever_1 = require("./reciever");
exports.createReciever = reciever_1.createReciever;
