import { DocumentSnapshot } from '@google-cloud/firestore';
import { CloudFunction } from 'firebase-functions';
import { JobModule, RecieverOptions } from './data-types';
/**
 * 指定した名前からモジュールを探索して返す
 *
 * この関数で探索する場合、次のルールで探索する
 *
 * - `@@` で始まる場合、このパッケージの定義済モジュールとみなす
 * -
 *
 * @param moduleName モジュール名
 * @returns モジュールオブジェクト or null(見つからない場合)
 */
export declare const findJob: (moduleName: string, basePaths?: string[] | undefined) => JobModule | null;
/**
 * ジョブ登録後のイベントハンドラ(実質的にジョブワーカーのエンドポイント)
 *
 * @param snapshot ジョブドキュメントのスナップショット
 * @param context
 */
export declare const createReciever: (options?: RecieverOptions) => CloudFunction<DocumentSnapshot>;
