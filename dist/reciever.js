"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*******************
 * ジョブの受け取り側領域
 */
const path = require("path");
const firebase_functions_1 = require("firebase-functions");
const constants_1 = require("./constants");
const utils_1 = require("./utils");
/**
 * 指定した名前からモジュールを探索して返す
 *
 * この関数で探索する場合、次のルールで探索する
 *
 * - `@@` で始まる場合、このパッケージの定義済モジュールとみなす
 * -
 *
 * @param moduleName モジュール名
 * @returns モジュールオブジェクト or null(見つからない場合)
 */
exports.findJob = (moduleName, basePaths) => {
    if (moduleName.startsWith('@@')) {
        try {
            const modulePath = path.join(__dirname, `./modules/${moduleName.slice(2)}`);
            return require(modulePath);
        }
        catch (err) {
            if (err.message.startsWith('Cannot find module')) {
                console.error(err.message);
                return null;
            }
            throw err;
        }
    }
    else if (basePaths) {
        for (let basePath of basePaths) {
            try {
                const modulePath = path.join(basePath, moduleName);
                return require(modulePath);
            }
            catch (err) {
            }
        }
    }
    return null;
};
/**
 * ジョブ登録後のイベントハンドラ(実質的にジョブワーカーのエンドポイント)
 *
 * @param snapshot ジョブドキュメントのスナップショット
 * @param context
 */
exports.createReciever = (options = {}) => {
    const modulePaths = options.modulePaths || [];
    modulePaths.push(`${__dirname}/modules`);
    const jobCollection = options.jobCollectioName || constants_1.JOB_COLLECTION;
    //
    const reciever = (jobSnap, context) => {
        // 初期設定
        const jobId = jobSnap.id;
        const job = jobSnap.data();
        console.log(`Recieve job-id: ${jobId}`);
        // モジュールの探索
        const module = exports.findJob(job.module, modulePaths);
        if (!module) {
            console.warn(`モジュールを名前から取得できませんでした: ${job.module}`);
            return;
        }
        // モジュール上での処理
        console.log(`Handle job with ${module.name}`);
        module.handle(job.payload || {});
        // 現時点では、処理の結果にかかわらず、登録ジョブを削除
        const docRef = utils_1.getFirebase().firestore().collection(constants_1.JOB_COLLECTION).doc(jobSnap.id);
        docRef.delete();
        return;
    };
    return firebase_functions_1.firestore.document(`${jobCollection}/{jobId}`).onCreate(reciever);
};
