import { app } from 'firebase-admin';
/**
 * UNIXタイムスタンプを取得する
 *
 * @returns タイムスタンプ
 */
export declare const getUnixtime: () => number;
/**
 * Firebaseのアプリケーションを取得・初期化するラッパースクリプト
 */
export declare const getFirebase: () => app.App;
