"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * ジョブ登録時の登録先Firestoreコレクション
 *
 */
exports.JOB_COLLECTION = 'jobs';
