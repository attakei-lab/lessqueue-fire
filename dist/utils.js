"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
/**
 * UNIXタイムスタンプを取得する
 *
 * @returns タイムスタンプ
 */
exports.getUnixtime = () => {
    return Math.floor((new Date()).getTime() / 1000);
};
/**
 * Firebaseのアプリケーションを取得・初期化するラッパースクリプト
 */
exports.getFirebase = () => {
    if (!admin.apps.length) {
        admin.initializeApp();
    }
    return admin.app();
};
