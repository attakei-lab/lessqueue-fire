"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/*******************
 * dispatch関数周りの処理
 */
const constants_1 = require("./constants");
const utils_1 = require("./utils");
const generateJobId = (moduleName) => {
    return `${moduleName.replace('/', '_')}__${utils_1.getUnixtime()}`;
};
/**
 * ジョブの登録
 *
 * FirestoreへのJob登録時ののIDは、モジュール名とタイムスタンプで一意化します
 *
 * @param job 実行したいアクションの情報
 */
exports.dispatch = (job) => __awaiter(void 0, void 0, void 0, function* () {
    const db = utils_1.getFirebase().firestore();
    const docRef = db.collection(constants_1.JOB_COLLECTION).doc(generateJobId(job.module));
    return docRef.set(job)
        .then(() => {
    });
});
