import { mocked } from 'ts-jest/utils'
import { app } from 'firebase-admin'
import { JOB_COLLECTION } from './constants'
import { Job } from './data-types'
import * as dispatcher from './dispatcher'
import { getFirebase } from './utils'

// eslint-disable-next-line @typescript-eslint/no-var-requires
const MockFirebase = require('mock-cloud-firestore')


jest.mock('./utils')


describe('dispatch()', () => {
  beforeEach(() => {
    mocked(getFirebase).mockClear()
  })

  it('基本実装', async (done) => {
    const firebase = new MockFirebase() as app.App
    mocked(getFirebase).mockImplementation(() => firebase)
    await dispatcher.dispatch({module: 'sss'})
    firebase.firestore().collection(JOB_COLLECTION).get()
      .then(qs => {
        expect(qs.size).toBe(1)
        expect(qs.docs[0].id.startsWith('sss__')).toBeTruthy()
        const job = qs.docs[0].data() as Job
        expect(job.module).toBe('sss')
        done()
      })
  })
})
