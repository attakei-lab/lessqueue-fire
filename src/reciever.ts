/*******************
 * ジョブの受け取り側領域
 */
import * as path from 'path'
import { DocumentSnapshot } from '@google-cloud/firestore'
import { EventContext, CloudFunction } from 'firebase-functions'
import { JOB_COLLECTION } from './constants'
import { Job, JobModule, JobSnapshot, RecieverOptions } from './data-types'
import { getFirebase } from './utils'


type AnyDocumentSnapshot = JobSnapshot | DocumentSnapshot


/**
 * 指定した名前からモジュールを探索して返す
 * 
 * この関数で探索する場合、次のルールで探索する
 * 
 * - `@@` で始まる場合、このパッケージの定義済モジュールとみなす
 * - 
 * 
 * @param moduleName モジュール名
 * @returns モジュールオブジェクト or null(見つからない場合)
 */
export const findJob = (moduleName: string, basePaths?: string[]): JobModule|null => {
  if (moduleName.startsWith('@@')) {
    try {
      const modulePath = path.join(__dirname, `./modules/${moduleName.slice(2)}`)
      return require(modulePath) as JobModule
    } catch (err) {
      if (err.message.startsWith('Cannot find module')) {
        console.error(err.message)
        return null
      }
      throw err
    }
  } else if(basePaths) {
    for (let basePath of basePaths) {
      try {
        const modulePath = path.join(basePath, moduleName)
        return require(modulePath) as JobModule
      } catch (err) {
      }  
    }
  }
  return null
}


/**
 * ジョブ登録後のイベントハンドラ(実質的にジョブワーカーのエンドポイント)
 * 
 * @param snapshot ジョブドキュメントのスナップショット
 * @param context 
 */
export const createReciever = (options: RecieverOptions={}): (snap: AnyDocumentSnapshot, ctx: EventContext) => void => {
  const modulePaths = options.modulePaths || []
  modulePaths.push(`${__dirname}/modules`)
  //
  return (jobSnap: AnyDocumentSnapshot, context: EventContext) => {
    // 初期設定
    const jobId = jobSnap.id
    const job = jobSnap.data() as Job
    console.log(`Recieve job-id: ${jobId}`)
    // モジュールの探索
    const module = findJob(job.module, modulePaths)
    if (!module) {
      console.warn(`モジュールを名前から取得できませんでした: ${job.module}`)
      return
    }
    // モジュール上での処理
    console.log(`Handle job with ${module.name}`)
    module.handle(job.payload || {})
    // 現時点では、処理の結果にかかわらず、登録ジョブを削除
    const docRef = getFirebase().firestore().collection(JOB_COLLECTION).doc(jobSnap.id)
    docRef.delete()
    return
  }
}


export const bindReciever = (options?: RecieverOptions): CloudFunction<DocumentSnapshot> => {
  const functions = require('firebase-functions')
  const opts = options || {}
  const jobCollection = opts.jobCollectioName || JOB_COLLECTION
  return functions.firestore.document(`${jobCollection}/{jobId}`).onCreate(createReciever(opts))
}
