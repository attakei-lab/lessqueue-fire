import { EventContext } from 'firebase-functions'
import { JobModule } from './data-types'
import * as reciever from './reciever'


describe('findJob()', () => {
  it('存在しないモジュール', () => {
    const module = reciever.findJob('-----')
    expect(module).toBeNull()
  })

  describe('予約済みモジュール', () => {
    it('version', () => {
      const module = reciever.findJob('@@version') as JobModule
      expect(module).not.toBeNull()
      expect(module.name).toBe('@@version')
    })

    it('versions(typo)', () => {
      const spyConsoleError = jest.spyOn(console, 'error')
      spyConsoleError.mockImplementation(x => x)
      const module = reciever.findJob('@@versions')
      expect(module).toBeNull()
      expect(console.error).toBeCalled()
    })
  })
})


describe('recieve()', () => {
  describe('予約済みモジュール', () => {
    it('version', () => {
      const spyConsoleLog = jest.spyOn(console, 'log')
      spyConsoleLog.mockImplementation(x => x)
      const snapshot = {
        id: '@@version__111',
        data() {
          return {module: '@@version'}
        },
      }
      reciever.createReciever()(snapshot, {} as EventContext)
      expect(console.log).toBeCalledWith('0.1.0')
    })
  })
})


