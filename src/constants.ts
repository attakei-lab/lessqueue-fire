/**
 * ジョブ登録時の登録先Firestoreコレクション
 * 
 */
export const JOB_COLLECTION = 'jobs'
