/**
 * Entrypoint for this package.
 *
 */
export { dispatch } from './dispatcher'
export { createReciever } from './reciever'
export { RecieverOptions } from './data-types'
