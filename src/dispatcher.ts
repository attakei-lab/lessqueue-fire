/*******************
 * dispatch関数周りの処理
 */
import { JOB_COLLECTION } from './constants'
import { Job } from './data-types'
import { getFirebase, getUnixtime } from './utils'



const generateJobId = (moduleName: string): string => {
  return `${moduleName.replace('/', '_')}__${getUnixtime()}`
}

/**
 * ジョブの登録
 *
 * FirestoreへのJob登録時ののIDは、モジュール名とタイムスタンプで一意化します
 * 
 * @param job 実行したいアクションの情報
 */
export const dispatch = async (job: Job): Promise<void> => {
  const db = getFirebase().firestore()
  const docRef = db.collection(JOB_COLLECTION).doc(generateJobId(job.module))
  return docRef.set(job)
    .then(() => {
    })
}
