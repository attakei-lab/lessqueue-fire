import * as admin from 'firebase-admin'
import { app } from 'firebase-admin'


/**
 * UNIXタイムスタンプを取得する
 *
 * @returns タイムスタンプ
 */
export const getUnixtime = (): number => {
  return Math.floor((new Date()).getTime() / 1000)
}


/**
 * Firebaseのアプリケーションを取得・初期化するラッパースクリプト
 */
export const getFirebase = (): app.App => {
  if (!admin.apps.length) {
    admin.initializeApp()
  }
  return admin.app()
}
