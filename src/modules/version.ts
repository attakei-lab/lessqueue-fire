/*******************
 * バージョンをどこかにポストするだけのモジュール
 */

export const name = '@@version'
export const description = 'バージョンをコンソール出力するだけのモジュール'


export const handle = async (): Promise<void> => {
  console.log('0.1.0')
}
