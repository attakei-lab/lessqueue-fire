export const name = 'hello'
export const description = 'コンソールにhello-worldを出力するだけのモジュール'


export const handle = async (): Promise<void> => {
  console.log('Hello world')
}
