import { QuerySnapshot } from '@google-cloud/firestore'
import { EventContext } from 'firebase-functions'
import { mocked } from 'ts-jest/utils'
import { JOB_COLLECTION } from '../src/constants'
import { dispatch } from '../src/dispatcher'
import { createReciever } from '../src/reciever'
import { getFirebase } from '../src/utils'

// eslint-disable-next-line @typescript-eslint/no-var-requires
const MockFirebase = require('mock-cloud-firestore')


jest.mock('../src/utils')

let spyConsoleLog: jest.SpyInstance


describe('Handle added job', () => {
  beforeEach(() => {
    spyConsoleLog = jest.spyOn(console, 'log')
    spyConsoleLog.mockImplementation(x => x)
  })

  afterEach(() => {
    spyConsoleLog.mockRestore()
    mocked(getFirebase).mockClear()
  })

  it('基本実装', async (done) => {
    const firebase = new MockFirebase()
    mocked(getFirebase).mockImplementation(() => firebase)
    await dispatch({module: 'version'})
    await firebase.firestore().collection(JOB_COLLECTION).get()
      .then((qs: QuerySnapshot) => {
        const snapshot = qs.docs[0]
        createReciever()(snapshot, {} as EventContext)
        expect(console.log).toBeCalledWith('0.1.0')
      })
      .catch(() => {
        done()
      })
    await firebase.firestore().collection(JOB_COLLECTION).get()
      .then((qs: QuerySnapshot) => {
        expect(qs.size).toBe(0)
      })
      .catch(() => {
        done()
      })
    done()
  })

  it('ローカルモジュール', async (done) => {
    const firebase = new MockFirebase()
    mocked(getFirebase).mockImplementation(() => firebase)
    await dispatch({module: 'hello'})
    await firebase.firestore().collection(JOB_COLLECTION).get()
      .then((qs: QuerySnapshot) => {
        const snapshot = qs.docs[0]
        createReciever({modulePaths: [`${__dirname}/modules`]})(snapshot, {} as EventContext)
        expect(console.log).toBeCalledWith('Hello world')
      })
      .catch(() => {
        done()
      })
    await firebase.firestore().collection(JOB_COLLECTION).get()
      .then((qs: QuerySnapshot) => {
        expect(qs.size).toBe(0)
      })
      .catch(() => {
        done()
      })
    done()
  })
})
